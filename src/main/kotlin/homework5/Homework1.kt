package homework5

class Lion(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("pork", "beef", "chicken")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Tiger(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("pork", "beef", "chicken")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Hippo(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("watermelon", "grass", "crackers")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Wolf(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("rabbit", "coffee", "chicken")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Giraffe(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("grass", "banana", "lettuce")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Elephant(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("grass", "banana", "wood")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Chimpanzee(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("grass", "banana", "chicken")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}

class Gorilla(val name: String, var growth: Int, var weight: Int) {
    var satiety = 1
    var foodPreferences: Array<String> = arrayOf("grass", "pork", "chicken")
    fun eat(food: String) {
        if (food.lowercase() in foodPreferences) {
            satiety += 1
            println("я покушал $food")
        } else {
            println("Я расстроен, не ем $food")
        }
    }
}
fun main() {
    val myLion = Lion("Петя", 1, 250)
    println("Введи, чем покормишь льва")
    val foodForLion = readln()
    myLion.eat(foodForLion)
    println("Сытость = ${myLion.satiety}")

    val myTiger = Tiger("Тигрила", 1, 400)
    println("Введи, чем покормишь тигра")
    val foodForTiger = readln()
    myTiger.eat(foodForTiger)
    println("Сытость = ${myTiger.satiety}")

    val myHippo = Hippo("Худышка", 2, 1000)
    println("Введи, чем покормишь бегемота")
    val foodForHippo = readln()
    myHippo.eat(foodForHippo)
    println("Сытость = ${myHippo.satiety}")

    val myWolf = Wolf("Серый", 2, 1000)
    println("Введи, чем покормишь волка")
    val foodForWolf = readln()
    myWolf.eat(foodForWolf)
    println("Сытость = ${myWolf.satiety}")

    val myGiraffe = Giraffe("Коротыш", 4, 1200)
    println("Введи, чем покормишь жирафа")
    val foodForGiraffe = readln()
    myGiraffe.eat(foodForGiraffe)
    println("Сытость = ${myGiraffe.satiety}")

    val myElephant = Elephant("Хобот", 3, 3000)
    println("Введи, чем покормишь слона")
    val foodForElephant = readln()
    myElephant.eat(foodForElephant)
    println("Сытость = ${myElephant.satiety}")

    val myChimpanzee = Chimpanzee("Кучерявый", 1, 70)
    println("Введи, чем покормишь шимпанзе")
    val foodForChimpanzee = readln()
    myChimpanzee.eat(foodForChimpanzee)
    println("Сытость = ${myChimpanzee.satiety}")

    val myGorilla = Gorilla("Гарри", 1, 250)
    println("Введи, чем покормишь гориллу")
    val foodForGorilla = readln()
    myGorilla.eat(foodForGorilla)
    println("Сытость = ${myGorilla.satiety}")
}