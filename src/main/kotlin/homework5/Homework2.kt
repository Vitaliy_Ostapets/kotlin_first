package homework5

fun main() {
    println("Введите число в диапозоне до 2 147 483 647")
    val digit = readLine()!!.toInt()
    var digitArray = digit.toString().toCharArray()
    var reverseString = ""
    for (i in digitArray.lastIndex downTo 0) {
        reverseString += (digitArray[i].toString())
    }
    println(reverseString.toInt())
}