package homework6

abstract class Animal(name: String, growth: Int, weight: Int) : Eatable {
    private val animalName = name
    var satiety = 1
    open lateinit var foodPreferences: Array<String>
    override fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
            println("$animalName покушал $food")
        } else {
            println("$animalName расстроен, не ем $food")
        }
    }

    fun feedAnimal() {
        println("Корми меня, я $animalName")
        val foodForAnimal = readln()
        this.eat(foodForAnimal)
        println("Сытость = ${this.satiety}")
    }
}

interface Eatable {
    fun eat(food: String)
}

class Lion(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("pork", "beef", "chicken")
}

class Tiger(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("pork", "beef", "chicken")
}

class Hippo(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("watermelon", "grass", "crackers")
}

class Wolf(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("rabbit", "coffee", "chicken")
}

class Giraffe(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {

    override var foodPreferences: Array<String> = arrayOf("grass", "banana", "lettuce")

}

class Elephant(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {

    override var foodPreferences: Array<String> = arrayOf("grass", "banana", "wood")
}

class Chimpanzee(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("grass", "banana", "chicken")
}

class Gorilla(private val name: String, var growth: Int, var weight: Int) : Animal(name, growth, weight) {
    override var foodPreferences: Array<String> = arrayOf("grass", "pork", "chicken")
}

fun main() {
    // Homework 5.1
    arrayOf(
        Lion("Петя", 1, 250),
        Tiger("Тигрила", 1, 400),
        Hippo("Худышка", 2, 1000),
        Wolf("Серый", 2, 1000),
        Giraffe("Коротыш", 4, 1200),
        Elephant("Хобот", 3, 3000),
        Chimpanzee("Кучерявый", 1, 70),
        Gorilla("Гарри", 1, 250)
    ).forEach {
        it.feedAnimal()
    }

    // Homework 5.2
    feed(
        arrayOf(Lion("Oleg", 1, 1), Tiger("Alesha", 2, 2)),
        arrayOf("chicken", "herb", "pork")
    )
}

// Homework 5.2
fun feed(animal: Array<Animal>, foods: Array<String>) {
    animal.forEach { animal ->
        foods.forEach {
            animal.eat(it)
        }
    }
}