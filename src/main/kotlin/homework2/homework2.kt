package homework2

fun main() {
    val lemonade: Int = 18500
    val pinaColada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val el: Double = 0.666666667
    val creative: String = "Что-то авторское!"

    println("Заказ $lemonade мл лимонада - готов!")
    println("Заказ $pinaColada мл пинаколады - готов!")
    println("Заказ $whiskey мл виски - готов!")
    println("Заказ $fresh капель фреша - готов! Это был самый долгий заказ.")
    println("Заказ $cola л колы - готов! Ваш безалкогольный виски-кола.")
    println("Заказ $el л эля - готов!")
    println("Заказ $creative - готов! Ну вы сами попросили...")
}