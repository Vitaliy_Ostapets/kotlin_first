package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var count2 = 0
    var count3 = 0
    var count4 = 0
    var count5 = 0
    var incorrectlyValues = 0
    for (element in marks) {
        when (element) {
            2 -> count2 += 1
            3 -> count3 += 1
            4 -> count4 += 1
            5 -> count5 += 1
            else -> incorrectlyValues += 1 // если в массиве элемент не равен 2, 3, 4, 5 - то проценты посчитаются некорректно (например, те кто не получал оценок и болел)
        }

    }
    println("двоечников ${"%.1f".format(((count2.toDouble() / (marks.size - incorrectlyValues)) * 100))} %")
    println("троечников ${"%.1f".format(((count3.toDouble() / (marks.size - incorrectlyValues)) * 100))} %")
    println("ударников ${"%.1f".format(((count4.toDouble() / (marks.size - incorrectlyValues)) * 100))} %")
    println("отличников ${"%.1f".format(((count5.toDouble() / (marks.size - incorrectlyValues)) * 100))} %")
}
